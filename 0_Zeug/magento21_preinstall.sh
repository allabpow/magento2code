#!/bin/bash
CUR_USERNAME="allapow"
INSTALLDIR="/var/www/magento21"

# Den User zur Gruppe des Webservers packen und seine Gruppen anzeigen
# sudo usermod -g www-data "$CUR_USERNAME"
# groups "$CUR_USERNAME"

cd "$INSTALLDIR"
# Set ownership and permissions for the shared group
# NUR wenn der Besitzer www-data ist!!!
# find var vendor pub/static pub/media app/etc -type f -exec chmod g+w {} \;
# find var vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} \;
# wenn der Besitzer NICHT www-data ist!!!
find . -type d -exec chmod 770 {} \; && sudo find . -type f -exec chmod 660 {} \;
chown -R :www-data .
chmod u+x bin/magento

# sudo service apache2 restart
