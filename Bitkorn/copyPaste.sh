##!/bin/bash

cd /var/www/magento205

php bin/magento cache:disable
php bin/magento cache:enable
php bin/magento cache:flush

php bin/magento setup:upgrade
php bin/magento setup:di:compile # dazu muss /var/di geloescht werden

sudo chown -R allapow:www-data *
# or before File-generation operations:
sudo su www-data

php bin/magento indexer:info
# nach reindex will der 'Page Cache' geflusht werden
php bin/magento indexer:reindex

######################
# Modul installieren
######################
php bin/magento module:enable --clear-static-content Vendor_Modulname
# The following modules have been enabled:
# - Bitkorn_Bitkbackend
# 
# To make sure that the enabled modules are properly registered, run 'setup:upgrade'.
# Cache cleared successfully.
# Generated classes cleared successfully. Please run the 'setup:di:compile' command to generate classes.
# Generated static view files cleared successfully.
# also (sonst Error in /var/report):
php bin/magento setup:upgrade
php bin/magento setup:di:compile

######################
# Set the Magento mode
# http://devdocs.magento.com/guides/v2.1/config-guide/cli/config-cli-subcommands-mode.html
######################
# If you’re changing from production mode to developer mode, delete the contents of the var/generation and var/di directories:
rm -rf <your Magento install dir>/var/di/* <your Magento install dir>/var/generation/* 
php bin/magento deploy:mode:show
php bin/magento deploy:mode:set developer
php bin/magento deploy:mode:set production --skip-compilation
# --skip-compilation is an optional parameter you can use to skip code compilation when you change to production mode.