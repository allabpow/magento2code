#!/bin/bash
magento2dir=/var/www/magento205

cd $magento2dir
bin/magento setup:upgrade
# Please re-run Magento compile command
# required only in production Mode
# in default or developer mode Magento 2 compiles the files automatically on the first page Load
# ...die Dateien die beim Page Load Verwendung finden!!!
# bin/magento setup:di:compile

# sudo chown -R allapow:www-data *
sudo chown -R allapow:www-data var/generation/*

# set permissions
# find . -type d -exec chmod 770 {} \; && find . -type f -exec chmod 660 {} \; && chmod u+x bin/magento

# ...oder anfangs mit:
# sudo su www-data
# die Dinge als ein anderer machen

# Inhalt von /pub/static neu erstellen
# bin/magento setup:static-content:deploy