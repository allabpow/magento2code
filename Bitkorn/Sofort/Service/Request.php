<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace Bitkorn\Sofort\Service;

/**
 * Description of Request
 *
 * @author bitkorn
 */
class Request {

    /**
     *
     * @var \Zend\Log\Logger 
     */
    private $logger;

    /**
     * 
     * @var \Zend\Http\Client
     */
    private $client;

    /**
     * Sofortueberweisung Konfigurationsdaten
     * 
     * @var array
     */
    private $config;
    
    /**
     *
     * @var string 
     */
    private $xmlRawBody;

    
    private function init() {
        if (!isset($this->config) || !is_array($this->config)) {
            throw new \Exception('config is corrupt');
        }
//        $this->client->setAuth(base64_encode($this->config['sofort_username']), base64_encode($this->config['sofort_passwd']));
        $this->client->setAuth($this->config['sofort_username'], $this->config['sofort_passwd']);
//        $this->client->setEncType('application/xml');
        $this->client->setUri($this->config['sofort_api_url']);
        $this->client->setMethod('post');
        try {
            $this->client->setHeaders(array(
                'Authorization' => 'Basic ' . base64_encode($this->config['sofort_username'] . ':' . $this->config['sofort_passwd']),
                'Content-Type' => 'application/xml; charset=UTF-8',
                'Accept' => 'application/xml; charset=UTF-8',
//                'Authorization: Basic ' . $this->authString,
//                'Content-Type: application/xml; charset=UTF-8',
//                'Accept: application/xml; charset=UTF-8',
            )); // hierauf sollte ein HTTP 200 OK folgen
            $this->client->setOptions(array(
                'timeout' => 10,
            ));
        } catch (\InvalidArgumentException $e) {
            $this->logger->log(\Zend\Log\Logger::ERR, $e);
        }
        
//        $this->logger->log(\Zend\Log\Logger::DEBUG, $this->client->getUri());
//        $headers = $this->client->getRequest()->getHeaders();
//        $this->logger->log(\Zend\Log\Logger::DEBUG, "\n" . $headers->toString());
    }
    
    /**
     * 
     * @return string XML Antwort
     */
    public function sendRequest() {
        if(!isset($this->xmlRawBody)) {
            throw new \Exception('xmlRawBody dont set!');
        }
        $this->init();
        
//        $this->logger->log(\Zend\Log\Logger::DEBUG, $this->xmlRawBody);

        $this->client->setRawBody($this->xmlRawBody);
//        $this->logger->log(\Zend\Log\Logger::DEBUG, $this->client->getRequest()->getContent()); // XML Content
        try {
            $response = $this->client->send();
            $this->logger->debug($response->getBody());
            if($response) {
                return $response->getBody();
            }
        } catch (Exception $e) {
            $this->logger->log(\Zend\Log\Logger::ERR, $e);
        }
        return FALSE;
    }

    public function setLogger(\Zend\Log\Logger $logger) {
        $this->logger = $logger;
    }

    public function setClient(\Zend\Http\Client $client) {
        $this->client = $client;
    }

    public function setConfig(array $config) {
        $this->config = $config;
    }

    public function setXmlRawBody($xmlRawBody) {
        $this->xmlRawBody = $xmlRawBody;
    }


}
