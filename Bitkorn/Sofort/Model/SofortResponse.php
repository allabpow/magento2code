<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace Bitkorn\Sofort\Model;

/**
 * Description of Sofort
 *
 * @author bitkorn
 */
class SofortResponse {

    private $valid = false;
    private $success = true;
    
    private $responseXmlString;
    /**
     *
     * @var \DOMDocument 
     */
    private $responseXml;
    private $newTransaction;

    /**
     *  DOMElement's
     */
    private $transaction;
    private $paymentUrl;
    private $warnings;
    private $warningArray = array();
    private $errors;
    private $errorArray = array();

    function __construct($responseXmlString) {
        $this->responseXmlString = $responseXmlString;
        $this->init();
    }

    private function init() {
        $this->responseXml = new \DOMDocument('1.0', 'UTF-8');
        if ($this->responseXml->loadXML($this->responseXmlString)) {
            $this->newTransaction = $this->responseXml->getElementsByTagName('new_transaction')->item(0);
            $this->transaction = $this->responseXml->getElementsByTagName('transaction')->item(0);
            $this->paymentUrl = $this->responseXml->getElementsByTagName('payment_url')->item(0);
            if($this->transaction instanceof \DOMNode && $this->paymentUrl instanceof \DOMNode) {
                $this->valid = TRUE;
            }
            
            $this->warnings = $this->responseXml->getElementsByTagName('warning');
            $this->errors = $this->responseXml->getElementsByTagName('error');
            foreach ($this->warnings as $warning) {
                $this->success = FALSE;
                $warnCode = $warning->getElementsByTagName('code')->item(0)->nodeValue;
                $warnMessage = $warning->getElementsByTagName('message')->item(0)->nodeValue;
                $warnField = $warning->getElementsByTagName('field')->item(0)->nodeValue;
                $this->warningArray[$warnCode] = $warnMessage . '; Field: ' . $warnField;
            }
            foreach ($this->errors as $error) {
                $this->success = FALSE;
                $code = $error->getElementsByTagName('code')->item(0)->nodeValue;
                $message = $error->getElementsByTagName('message')->item(0)->nodeValue;
                $field = $error->getElementsByTagName('field')->item(0)->nodeValue;
                $this->errorArray[$code] =  $message . '; Field: ' . $field;
            }
        } else {
            $this->valid = FALSE;
        }
    }

    public function isValid() {
        return $this->valid;
    }

    public function isSuccess() {
        return $this->success;
    }


    public function getTransaction() {
        if($this->valid) {
            return $this->transaction->nodeValue;
        }
    }
    
    public function getPaymentUrl() {
        if($this->valid) {
            return $this->paymentUrl->nodeValue;
        }
    }

    public function getWarningArray() {
        return $this->warningArray;
    }

    public function getErrorArray() {
        return $this->errorArray;
    }

    function getResponseXmlString() {
        return $this->responseXmlString;
    }


}
