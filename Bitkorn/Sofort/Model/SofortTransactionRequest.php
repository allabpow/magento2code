<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace Bitkorn\Sofort\Model;

/**
 * Description of SofortTransactionRequest
 *
 * @author bitkorn
 */
class SofortTransactionRequest {

    
    /**
     *
     * @var \Zend\Log\Logger
     */
    private $logger;
    
    /**
     *
     * @var \DOMDocument 
     */
    private $xmlDoc;

    /**
     *  DOMElement's
     */
    private $transactionRequest; // root Element
    private $transactions; // Array with Transaction DOMElement's
    /* or */
    private $fromTime; // from_time
    private $toTime; // to_time
    private $fromStatusModifiedTime; // from_status_modified_time
    private $toStatusModifiedTime; // to_status_modified_time
    private $status;
    private $statusReason; // status_reason
    private $number;
    private $page;

    function __construct() {
        $this->xmlDoc = new \DOMDocument('1.0', 'UTF-8');
        $this->transactionRequest = $this->xmlDoc->createElement('transaction_request');
        $this->transactionRequest->setAttribute('version', 2);
        $this->xmlDoc->appendChild($this->transactionRequest);
    }

    /**
     * [0..100]
     * transaction's only works alone without from_time or to_time etc
     * @param string $transactions
     */
    public function setTransactions(array $transactions) {
        if(count($transactions) > 100) {
            throw new \Exception('to many transactions, max 100!');
        }
        $this->removeOther(2);
        if (isset($this->transactions)) {
            foreach ($this->transactions as $transac) {
                $this->xmlDoc->removeChild($transac);
            }
        }
        foreach ($transactions as $transaction) {
            $tmpTransaction = $this->xmlDoc->createElement('transaction');
            $tmpTransaction->nodeValue = $transaction;
            $this->transactionRequest->appendChild($tmpTransaction);
            $this->transactions[] = $tmpTransaction;
        }
    }

    /**
     * 
     * @param string $fromDate as 0000-00-00
     * @param string $fromTime as 00:00:00
     * @param string $timeZoneOffset as +02:00 or -04:00
     */
    public function setFromTime($fromDate, $fromTime = '00:00:00', $timeZoneOffset = '+02:00') {
        try {
            $dateTime = new \DateTime($fromDate . ' ' . $fromTime . $timeZoneOffset);
        } catch(\Exception $e) {
            $this->logger->log(\Zend\Log\Logger::ERR, $e);
        }
        $this->removeOther(1);
        if(!isset($this->fromTime)) {
            $this->fromTime = $this->xmlDoc->createElement('from_time');
            $this->transactionRequest->appendChild($this->fromTime);
        }
        $this->fromTime->nodeValue = $dateTime->format('Y-m-d\TH:i:sO'); // ISO 8601
    }

    /**
     * 
     * @param string $toDate as 0000-00-00
     * @param string $toTime as 00:00:00
     * @param string $timeZoneOffset as +02:00 or -04:00
     */
    public function setToTime($toDate, $toTime = '00:00:00', $timeZoneOffset = '+02:00') {
        try {
            $dateTime = new \DateTime($toDate . ' ' . $toTime . $timeZoneOffset);
        } catch(\Exception $e) {
            $this->logger->log(\Zend\Log\Logger::ERR, $e);
        }
        $this->removeOther(1);
        if(!isset($this->toTime)) {
            $this->toTime = $this->xmlDoc->createElement('to_time');
            $this->transactionRequest->appendChild($this->toTime);
        }
        $this->toTime->nodeValue = $dateTime->format('Y-m-d\TH:i:sO'); // ISO 8601
    }

    /**
     * 
     * @param string $fromDate as 0000-00-00
     * @param string $fromTime as 00:00:00
     * @param string $timeZoneOffset as +02:00 or -04:00
     */
    public function setFromStatusModifiedTime($fromDate, $fromTime = '00:00:00', $timeZoneOffset = '+02:00') {
        try {
            $dateTime = new \DateTime($fromDate . ' ' . $fromTime . $timeZoneOffset);
        } catch(\Exception $e) {
            $this->logger->log(\Zend\Log\Logger::ERR, $e);
        }
        $this->removeOther(1);
        if(!isset($this->fromStatusModifiedTime)) {
            $this->fromStatusModifiedTime = $this->xmlDoc->createElement('from_status_modified_time');
            $this->transactionRequest->appendChild($this->fromStatusModifiedTime);
        }
        $this->fromStatusModifiedTime->nodeValue = $dateTime->format('Y-m-d\TH:i:sO'); // ISO 8601
    }

    /**
     * 
     * @param string $toDate as 0000-00-00
     * @param string $toTime as 00:00:00
     * @param string $timeZoneOffset as +02:00 or -04:00
     */
    public function setToStatusModifiedTime($toDate, $toTime = '00:00:00', $timeZoneOffset = '+02:00') {
        try {
            $dateTime = new \DateTime($toDate . ' ' . $toTime . $timeZoneOffset);
        } catch(\Exception $e) {
            $this->logger->log(\Zend\Log\Logger::ERR, $e);
        }
        $this->removeOther(1);
        if(!isset($this->toStatusModifiedTime)) {
            $this->toStatusModifiedTime = $this->xmlDoc->createElement('to_status_modified_time');
            $this->transactionRequest->appendChild($this->toStatusModifiedTime);
        }
        $this->toStatusModifiedTime->nodeValue = $dateTime->format('Y-m-d\TH:i:sO'); // ISO 8601
    }

    public function setStatus($status) {
        if (!isset($this->status)) {
            $this->status = $this->xmlDoc->createElement('status');
            $this->transactionRequest->appendChild($this->status);
        }
        $this->status->nodeValue = $status;
    }

    public function setStatusReason($statusReason) {
        if (!isset($this->statusReason)) {
            $this->statusReason = $this->xmlDoc->createElement('status_reason');
            $this->transactionRequest->appendChild($this->statusReason);
        }
        $this->statusReason->nodeValue = $statusReason;
    }

    /**
     * Count of transactions to fetch.
     * @param int $number
     */
    public function setNumber($number) {
        $number = (int)$number;
        if (!isset($this->number)) {
            $this->number = $this->xmlDoc->createElement('number');
            $this->transactionRequest->appendChild($this->number);
        }
        $this->number->nodeValue = $number;
    }

    /**
     * If number set you can specify the page.
     * e.g. number=10, page=2 results in transactions 11-20
     * @param int $page
     */
    public function setPage($page) {
        $page = (int)$page;
        if (!isset($this->page)) {
            $this->page = $this->xmlDoc->createElement('page');
            $this->transactionRequest->appendChild($this->page);
        }
        $this->page->nodeValue = $page;
    }

                
    /**
     * 
     * @return string XML content for the request body.
     */
    public function getXmlContent() {
        return $this->xmlDoc->saveXML();
    }

    /**
     * 
     * @param int $wich 1=remove transaction; 2=remove all other
     */
    private function removeOther($wich) {
        switch ($wich) {
            case 1:
                if(isset($this->transactions)) {
                    foreach ($this->transactions as $transaction) {
                        $this->transactionRequest->removeChild($transaction);
                    }
                    unset($this->transactions);
                }
                break;
            case 2:
                if(isset($this->fromTime)) {
                    $this->transactionRequest->removeChild($this->fromTime);
                    unset($this->fromTime);
                }
                if(isset($this->toTime)) {
                    $this->transactionRequest->removeChild($this->toTime);
                    unset($this->toTime);
                }
                if(isset($this->fromStatusModifiedTime)) {
                    $this->transactionRequest->removeChild($this->fromStatusModifiedTime);
                    unset($this->fromStatusModifiedTime);
                }
                if(isset($this->toStatusModifiedTime)) {
                    $this->transactionRequest->removeChild($this->toStatusModifiedTime);
                    unset($this->toStatusModifiedTime);
                }
                if(isset($this->status)) {
                    $this->transactionRequest->removeChild($this->status);
                    unset($this->status);
                }
                if(isset($this->statusReason)) {
                    $this->transactionRequest->removeChild($this->statusReason);
                    unset($this->statusReason);
                }
                if(isset($this->number)) {
                    $this->transactionRequest->removeChild($this->number);
                    unset($this->number);
                }
                if(isset($this->page)) {
                    $this->transactionRequest->removeChild($this->page);
                    unset($this->page);
                }
                break;
        }
    }

    public function setLogger(\Zend\Log\Logger $logger) {
        $this->logger = $logger;
    }

}
