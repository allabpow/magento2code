<?php

/*
 * Von was soll sie erben???
 * \Magento\Payment\Model\Method\Cc
 * UND
 * \Magento\Payment\Model\Method\AbstractMethod
 * sind BEIDE DEPRECATED!
 * 
 * Aber diese beiden werden in Tutorials verwendet
 */

namespace Bitkorn\Sofort\Model;

/**
 * Description of Sofort
 *
 * @author allapow
 */
class Sofort implements \Magento\Payment\Model\MethodInterface {

    public function acceptPayment(\Magento\Payment\Model\InfoInterface $payment) {
        
    }

    public function assignData(\Magento\Framework\DataObject $data) {
        
    }

    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount) {
        
    }

    public function canAuthorize() {
        
    }

    public function canCapture() {
        
    }

    public function canCaptureOnce() {
        
    }

    public function canCapturePartial() {
        
    }

    public function canEdit() {
        
    }

    public function canFetchTransactionInfo() {
        
    }

    public function canOrder() {
        
    }

    public function canRefund() {
        
    }

    public function canRefundPartialPerInvoice() {
        
    }

    public function canReviewPayment() {
        
    }

    public function canUseCheckout() {
        
    }

    public function canUseForCountry($country) {
        
    }

    public function canUseForCurrency($currencyCode) {
        
    }

    public function canUseInternal() {
        
    }

    public function canVoid() {
        
    }

    public function cancel(\Magento\Payment\Model\InfoInterface $payment) {
        
    }

    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount) {
        
    }

    public function denyPayment(\Magento\Payment\Model\InfoInterface $payment) {
        
    }

    public function fetchTransactionInfo(\Magento\Payment\Model\InfoInterface $payment, $transactionId) {
        
    }

    public function getCode() {
        
    }

    public function getConfigData($field, $storeId = null) {
        
    }

    public function getConfigPaymentAction() {
        
    }

    public function getFormBlockType() {
        
    }

    public function getInfoBlockType() {
        
    }

    public function getInfoInstance() {
        
    }

    public function getStore() {
        
    }

    public function getTitle() {
        
    }

    public function initialize($paymentAction, $stateObject) {
        
    }

    public function isActive($storeId = null) {
        
    }

    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null) {
        
    }

    public function isGateway() {
        
    }

    public function isInitializeNeeded() {
        
    }

    public function isOffline() {
        
    }

    public function order(\Magento\Payment\Model\InfoInterface $payment, $amount) {
        
    }

    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount) {
        
    }

    public function setInfoInstance(\Magento\Payment\Model\InfoInterface $info) {
        
    }

    public function setStore($storeId) {
        
    }

    public function validate() {
        
    }

    public function void(\Magento\Payment\Model\InfoInterface $payment) {
        
    }

}
