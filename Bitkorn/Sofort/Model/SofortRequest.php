<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace Bitkorn\Sofort\Model;

/**
 * Base class for Sofortueberweisung
 *
 * @author bitkorn
 */
class SofortRequest {
    
    /**
     *
     * @var \Zend\Log\Logger
     */
    private $logger;
    
    /**
     *
     * @var \DOMDocument 
     */
    private $doc;

    /**
     *  DOMElement's
     */
    /* root Element */
    private $multipay;
    /*  */
    private $projectId;
    /* Sender */
    private $sender;
    private $holder;
    private $accountNumber;
    private $bankCode;
    /*  */
    private $amount;
    private $currencyCode; // currency_code
    /* Reasons */
    private $reasons;
    private $reason1; // reason
    private $reason2; // reason
    /*  */
    private $su;
    // ####### Optionale XML Request Tags #######
    private $languageCode;
    private $interfaceVersion;
    private $timeout;
    private $emailCustomer;
    private $phoneCustomer;
    private $userVariables;
    private $successUrl;
    private $successLinkRedirect;
    private $abortUrl;
    private $timeoutUrl;
    private $notificationUrls;
    private $notificationEmails;
    private $customerProtection;

    public function __construct() {
        $this->doc = new \DOMDocument('1.0', 'UTF-8'); // <multipay></multipay>
        $this->multipay = $this->doc->createElement('multipay');
        $this->doc->appendChild($this->multipay);
        $this->sender = $this->doc->createElement('sender');
        $this->multipay->appendChild($this->sender);
        $this->reasons = $this->doc->createElement('reasons');
        $this->multipay->appendChild($this->reasons);
        $this->su = $this->doc->createElement('su');
        $this->multipay->appendChild($this->su);
    }

    /**
     * [1]
     * 
     * @param type $projectId
     */
    public function setProjectId($projectId) {
        if (!isset($this->projectId)) {
            $this->projectId = $this->doc->createElement('project_id');
            $this->multipay->appendChild($this->projectId);
        }
        $this->projectId->nodeValue = $projectId;
    }

    /**
     * [1]
     * Kontoinhaber der zahlen will.
     * Element von sender
     * 
     * @param type $holder holder: Vor- und Nachname
     * @param type $accountNumber account_number: Kto.-Nr.
     * @param type $bankCode bank_code: BLZ
     */
    public function setHolder($holder, $accountNumber, $bankCode) {
        if (!isset($this->holder)) {
            $this->holder = $this->doc->createElement('holder');
            $this->sender->appendChild($this->holder);
        }
        if (!isset($this->accountNumber)) {
            $this->accountNumber = $this->doc->createElement('account_number');
            $this->sender->appendChild($this->accountNumber);
        }
        if (!isset($this->bankCode)) {
            $this->bankCode = $this->doc->createElement('bank_code');
            $this->sender->appendChild($this->bankCode);
        }
        $this->holder->nodeValue = $holder;
        $this->accountNumber->nodeValue = $accountNumber;
        $this->bankCode->nodeValue = $bankCode;
    }

    /**
     * [1]
     * 
     * @param type $amount
     */
    public function setAmount($amount) {
        if (!isset($this->amount)) {
            $this->amount = $this->doc->createElement('amount');
            $this->multipay->appendChild($this->amount);
        }
        $this->amount->nodeValue = $amount;
    }

    /**
     * [1]
     * 
     * @param type $currencyCode
     */
    public function setCurrencyCode($currencyCode = 'EUR') {
        if (!isset($this->currencyCode)) {
            $this->currencyCode = $this->doc->createElement('currency_code');
            $this->multipay->appendChild($this->currencyCode);
        }
        $this->currencyCode->nodeValue = $currencyCode;
    }

    /**
     * [1][1..2]
     * anders als in der Sofort-Doku beschrieben, darf reason.1 nicht leer sein
     * <error>
     *   <code>8010</code>
     *   <message>Must not be empty.</message>
     *   <field>reasons.reason.1</field>
     * </error>
     * 
     * @param type $reason
     */
    public function setReasons($reason1, $reason2 = '') {
        if (!isset($this->reason1)) {
            $this->reason1 = $this->doc->createElement('reason');
            $this->reasons->appendChild($this->reason1);
        }
        $this->reason1->nodeValue = $reason1;
        
        if($reason2) {
            if (!isset($this->reason2)) {
                $this->reason2 = $this->doc->createElement('reason');
                $this->reasons->appendChild($this->reason2);
            }
            $this->reason2->nodeValue = $reason2;
        }
    }

    // ############################## Optionale XML Request Tags ####################################
    
    /**
     * [0,1]
     * @param type $languageCode
     */
    public function setLanguageCode($languageCode) {
        if (!isset($this->languageCode)) {
            $this->languageCode = $this->doc->createElement('language_code');
            $this->multipay->appendChild($this->languageCode);
        }
        $this->languageCode->nodeValue = $languageCode;
    }
    
    /**
     * [0,1]
     * @param type $interfaceVersion
     */
    public function setInterfaceVersion($interfaceVersion) {
        if (!isset($this->interfaceVersion)) {
            $this->interfaceVersion = $this->doc->createElement('interface_version');
            $this->multipay->appendChild($this->interfaceVersion);
        }
        $this->interfaceVersion->nodeValue = $interfaceVersion;
    }
    
    /**
     * [0,1]
     * @param type $timeout
     */
    public function setTimeout($timeout) {
        if (!isset($this->timeout)) {
            $this->timeout = $this->doc->createElement('timeout');
            $this->multipay->appendChild($this->timeout);
        }
        $this->timeout->nodeValue = $timeout;
    }
    
    /**
     * [0,1]
     * @param type $emailCustomer
     */
    public function setEmailCustomer($emailCustomer) {
        if (!isset($this->emailCustomer)) {
            $this->emailCustomer = $this->doc->createElement('email_customer');
            $this->multipay->appendChild($this->emailCustomer);
        }
        $this->emailCustomer->nodeValue = $emailCustomer;
    }

    /**
     * [0,1]
     * @param type $phoneCustomer
     */
    public function setPhoneCustomer($phoneCustomer) {
        if (!isset($this->phoneCustomer)) {
            $this->phoneCustomer = $this->doc->createElement('phone_customer');
            $this->multipay->appendChild($this->phoneCustomer);
        }
        $this->phoneCustomer->nodeValue = $phoneCustomer;
    }

    /**
     * [1]?![0..20]
     * @param array $userVariables array('var1','var2','var3', ...)
     * @throws Exception
     */
    public function setUserVariables(array $userVariables) {
        if(count($userVariables) > 20) {
            throw new Exception('To many userVariables, max 20!');
        }
        if (isset($this->userVariables)) {
            $this->multipay->removeChild($this->userVariables);
            unset($this->userVariables);
        }
        $this->userVariables = $this->doc->createElement('user_variables');
        $this->multipay->appendChild($this->userVariables);
        foreach ($userVariables as $userVariable) {
            $xmlUserVar = $this->doc->createElement('user_variable');
            $xmlUserVar->nodeValue = $userVariable;
            $this->userVariables->appendChild($xmlUserVar);
        }
    }

    /**
     * [0,1]
     * @param type $successUrl
     */
    public function setSuccessUrl($successUrl) {
        if (!isset($this->successUrl)) {
            $this->successUrl = $this->doc->createElement('success_url');
            $this->multipay->appendChild($this->successUrl);
        }
        $this->successUrl->nodeValue = $successUrl;
    }

    /**
     * [0,1]
     * @param type $successLinkRedirect
     */
    public function setSuccessLinkRedirect($successLinkRedirect) {
        if (!isset($this->successLinkRedirect)) {
            $this->successLinkRedirect = $this->doc->createElement('success_link_redirect');
            $this->multipay->appendChild($this->successLinkRedirect);
        }
        $this->successLinkRedirect->nodeValue = $successLinkRedirect;
    }

    /**
     * [0,1]
     * @param type $abortUrl
     */
    public function setAbortUrl($abortUrl) {
        if (!isset($this->abortUrl)) {
            $this->abortUrl = $this->doc->createElement('abort_url');
            $this->multipay->appendChild($this->abortUrl);
        }
        $this->abortUrl->nodeValue = $abortUrl;
    }

    /**
     * [0,1]
     * @param type $timeoutUrl
     */
    public function setTimeoutUrl($timeoutUrl) {
        if (!isset($this->timeoutUrl)) {
            $this->timeoutUrl = $this->doc->createElement('timeout_url');
            $this->multipay->appendChild($this->timeoutUrl);
        }
        $this->timeoutUrl->nodeValue = $timeoutUrl;
    }

    /**
     * [0,1][0..5]
     * 
     * @param array $notificationUrls array('notifyUrl1',array('notifyUrl2','attribute notify_on'),array('notifyUrl3','attributeVal1,attributeVal2'), ...)
     * @throws Exception
     */
    public function setNotificationUrls(array $notificationUrls) {
        if(count($notificationUrls) > 5) {
            throw new Exception('To many notificationUrls, max 5!');
        }
        if (isset($this->notificationUrls)) {
            $this->multipay->removeChild($this->notificationUrls);
            unset($this->notificationUrls);
        }
        $this->notificationUrls = $this->doc->createElement('notification_urls');
        $this->multipay->appendChild($this->notificationUrls);
        foreach ($notificationUrls as $notificationUrl) {
            $xmlNotifyUrl = $this->doc->createElement('notification_url');
            $this->notificationUrls->appendChild($xmlNotifyUrl);
            if(is_array($notificationUrl)) {
                $xmlNotifyUrl->nodeValue = $notificationUrl[0];
                $xmlNotifyUrl->setAttribute('notify_on', $notificationUrl[1]);
            } else {
                $xmlNotifyUrl->nodeValue = $notificationUrl;
            }
        }
    }

    /**
     * [0,1][0..5]
     * @param array $notificationEmails array('notifyEmail1',array('notifyEmail2','attributeVal'),array('notifyEmail3','attributeVal1,attributeVal2'), ...)
     * @throws Exception
     */
    public function setNotificationEmails($notificationEmails) {
        if(count($notificationEmails) > 10) {
            throw new Exception('To many notificationEmails, max 10!');
        }
        if (isset($this->notificationEmails)) {
            $this->multipay->removeChild($this->notificationEmails);
            unset($this->notificationEmails);
        }
        $this->notificationEmails = $this->doc->createElement('notification_emails');
        $this->multipay->appendChild($this->notificationEmails);
        foreach ($notificationEmails as $notificationEmail) {
            $xmlNotifyEmail = $this->doc->createElement('notification_email');
            $this->notificationEmails->appendChild($xmlNotifyEmail);
            if(is_array($notificationEmail)) {
                $xmlNotifyEmail->nodeValue = $notificationEmail[0];
                $xmlNotifyEmail->setAttribute('notify_on', $notificationEmail[1]);
            } else {
                $xmlNotifyEmail->nodeValue = $notificationEmail;
            }
        }
    }

    /**
     * [0,1]
     * @param type $customerProtection
     */
    public function setCustomerProtection($customerProtection) {
        if (!isset($this->customerProtection)) {
            $this->customerProtection = $this->doc->createElement('customer_protection');
            $this->su->appendChild($this->customerProtection);
        }
        $this->customerProtection->nodeValue = (int)$customerProtection;
    }

                
    /**
     * 
     * @return string XML content for the request body.
     */
    public function getXmlContent() {
        return $this->doc->saveXML();
    }

    public function setLogger(\Zend\Log\Logger $logger) {
        $this->logger = $logger;
    }


}
