<?php

namespace Bitkorn\Office\Model\ResourceModel;

/**
 * Description of Employee
 *
 * @author allapow
 */
class Employee extends \Magento\Eav\Model\Entity\AbstractEntity {

    protected function _construct() {
        // set the $this->_read, $this->_write class properties
        // These are freely assigned to whatever value we want, preferably following the naming pattern of our module.
        $this->_read  = 'bitkorn_office_employee_read';
        $this->_write = 'bitkorn_office_employee_write';
    }

    public function getEntityType() {
        if (empty($this->_type)) {
            $this->setType(\Bitkorn\Office\Model\Employee::ENTITY);
        }
        return parent::getEntityType();
    }

}
