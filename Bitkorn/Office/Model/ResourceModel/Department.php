<?php

namespace Bitkorn\Office\Model\ResourceModel;

/**
 * Description of Department
 *
 * @author allapow
 */
class Department extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    
    protected function _construct() {
        // The first parameter is the table name bitkorn_office_department, where our model will persist its data.
        // The second parameter is the primary column name entity_id within that table.
        $this->_init('bitkorn_office_department', 'entity_id');
    }

}
