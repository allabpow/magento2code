<?php

namespace Bitkorn\Office\Model\ResourceModel\Department;

/**
 * Description of Collection
 *
 * @author allapow
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected function _construct() {
        // erster Parameter ist das einfache Model
        // zweiter Parameter ist das ResourceModel
        $this->_init(
                'Bitkorn\Office\Model\Department', 'Bitkorn\Office\Model\ResourceModel\Department'
        );
    }

}
