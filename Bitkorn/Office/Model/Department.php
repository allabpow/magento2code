<?php

namespace Bitkorn\Office\Model;

/**
 * A Simple Model (no EAV Model)
 * 
 * The AbstractModel extends \Magento\Framework\Object. The fact that our model class ultimately extends from Object
 * means that we do not have to define a property name on our model class. What Object does for us is that it enables us to
 * get, set, unset, and check for a value existence on properties magically.
 *
 * @author allapow
 */
class Department extends \Magento\Framework\Model\AbstractModel {
    
    protected function _construct() {
        // das einfache Model initialisiert sich mit seinem ResourceModel
        $this->_init('Bitkorn\Office\Model\ResourceModel\Department');
    }

}
