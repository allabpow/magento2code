<?php

namespace Bitkorn\Office\Model;

/**
 * EAV Model
 * Also extending from \Magento\Framework\Model\AbstractModel like in the simple Model Department.
 *
 * @author allapow
 */
class Employee extends \Magento\Framework\Model\AbstractModel {

    /**
     * @var string This value is what's stored in the entity_type_code column within the eav_entity_type table
     */
    const ENTITY = 'bitkorn_office_employee';

    public function _construct() {
        // auch das EAV Model initialisiert sich mit seinem ResourceModel
        $this->_init('Bitkorn\Office\Model\ResourceModel\Employee');
    }

}
