<?php

/*
 * no license
 */

namespace Bitkorn\Office\Controller\Test;

/**
 * The Controller action class is basically just an extension of the controller defining the execute method.
 * Code within the execute method is what gets run when we hit the URL in the browser.
 * Additionally, we have a __construct method to which we are passing the EmployeeFactory and DepartmentFactory classes,
 * which we will soon use for our CRUD examples.
 * Note that EmployeeFactory and DepartmentFactory are not classes created by us. Magento will autogenerate them
 * within the var/generation/Bitkorn/Office/Model folder. These are factory classes for our Employee and Department model classes,
 * generated when requested.
 *
 * @author allapow
 */
class Crud extends \Bitkorn\Office\Controller\Test {

    protected $employeeFactory;
    protected $departmentFactory;
    protected $resultPageFactory;

    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Bitkorn\Office\Model\EmployeeFactory $employeeFactory, \Bitkorn\Office\Model\DepartmentFactory $departmentFactory,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->employeeFactory = $employeeFactory;
        $this->departmentFactory = $departmentFactory;
        $this->resultPageFactory = $resultPageFactory;
        return parent::__construct($context);
    }

    /**
     * http://magento200examples.local/bitkorn_office/Test/crud
     */
    public function execute() {
        $resultPage = $this->resultPageFactory->create();
        /*
         * ohne /app/code/Bitkorn/Office/view/frontend/layout/bitkorn_office_test_crud.xml funzt das hier NICHT
         * plus bin/magento setup:upgrade
         * 
         * angezeigt werden die Messages nur wenn obige Layout Datei mal nicht da war
         * ...dann wieder rein und bin/magento setup:upgrade und lauter Messages werden angezeigt.
         * ...KOMISCH
         */
        $this->messageManager->addSuccess('Success-1');
        $this->messageManager->addSuccess('Success-2');
        $this->messageManager->addNotice('Notice-1');
        $this->messageManager->addNotice('Notice-2');
        $this->messageManager->addWarning('Warning-1');
        $this->messageManager->addWarning('Warning-2');
        $this->messageManager->addError('Error-1');
        $this->messageManager->addError('Error-2');
        return $resultPage;
        
        //Simple model, creating new entities, flavour #1
        $department1 = $this->departmentFactory->create();
        $department1->setName('Finance');
        $department1->save();
        //Simple model, creating new entities, flavour #2
        $department2 = $this->departmentFactory->create();
        $department2->setData('name', 'Research');
        $department2->save();
        //Simple model, creating new entities, flavour #3
        $department3 = $this->departmentFactory->create();
        $department3->setData(['name' => 'Support']);
        $department3->save();
        /*
         * The flavour #1 approach from the preceding code is probably the preferred way of setting properties,
         * as it is using the magic method approach we mentioned previously.
         * Both flavour #2 and flavour #3 use the setData method, just in a slightly different manner.
         * All three examples should yield the same result once the save method is called on an object instance.
         */

        /*
         * EAV model, creating new entities, flavour #1
         */
        $employee1 = $this->employeeFactory->create();
        $employee1->setDepartment_id($department1->getId());
        $employee1->setEmail('goran@mail.loc');
        $employee1->setFirstName('Goran');
        $employee1->setLastName('Gorvat');
        // ab hier attributes
        // Base table or view not found: 1146 Table 'magento200examples.bitkorn_office_employee_entity_int' doesn't exist, query was: DESCRIBE `bitkorn_office_employee_entity_int`
        // Loesung: in \Bitkorn\Office\Setup\InstallSchema die bitkorn_office_employee_entity_* Tables erzeugen
        $employee1->setServiceYears(3);
        $employee1->setDob('1984-04-18');
        $employee1->setSalary(3800.00);
        $employee1->setVatNumber('GB123451234');
        $employee1->setNote('Note #1');
        $employee1->save();

        /*
         * EAV model, creating new entities, flavour #2
         */
        $employee2 = $this->employeeFactory->create();
        $employee2->setData('department_id', $department2->getId());
        $employee2->setData('email', 'marko@mail.loc');
        $employee2->setData('first_name', 'Marko');
        $employee2->setData('last_name', 'Tunukovic');
        // ab hier attributes
        // Base table or view not found: 1146 Table 'magento200examples.bitkorn_office_employee_entity_int' doesn't exist, query was: DESCRIBE `bitkorn_office_employee_entity_int`
        // Loesung: in \Bitkorn\Office\Setup\InstallSchema die bitkorn_office_employee_entity_* Tables erzeugen
        $employee2->setData('service_years', 3);
        $employee2->setData('dob', '1984-04-18');
        $employee2->setData('salary', 3800.00);
        $employee2->setData('vat_number', 'GB123451234');
        $employee2->setData('note', 'Note #2');
        $employee2->save();

        /*
         * EAV model, creating new entities, flavour #3
         */
        $employee3 = $this->employeeFactory->create();
        $employee3->setData([
            'department_id' => $department3->getId(),
            'email' => 'ivan@mail.loc',
            'first_name' => 'Ivan',
            'last_name' => 'Telebar',
            // ab hier attributes
            // Base table or view not found: 1146 Table 'magento200examples.bitkorn_office_employee_entity_int' doesn't exist, query was: DESCRIBE `bitkorn_office_employee_entity_int`
            // Loesung: in \Bitkorn\Office\Setup\InstallSchema die bitkorn_office_employee_entity_* Tables erzeugen
            'service_years' => 2,
            'dob' => '1986-08-22',
            'salary' => 2400.00,
            'vat_number' => 'GB123454321',
            'note' => 'Note #3'
        ]);
        $employee3->save();

        /*
         * Reading existing entities
         * There is no real difference between loading the simple model or EAV model.
         */
        // Simple model, reading existing entities
        $department = $this->departmentFactory->create();
        $department->load(28);
        \Zend_Debug::dump($department->toArray()); // ZF1
        // EAV model, reading existing entities
        $employee = $this->employeeFactory->create();
        $employee->load(25);
        \Zend_Debug::dump($employee->toArray()); // ZF1

        /*
         * Updating existing entities
         */
//        $department = $this->departmentFactory->create();
//        $department->load(28);
//        $department->setName('Finance #2');
//        $department->save();
    }

}
