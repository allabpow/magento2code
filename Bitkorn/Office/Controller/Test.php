<?php

/*
 * no license
 */

namespace Bitkorn\Office\Controller;

/**
 * This really is the simplest controller we could have defined.
 * The only thing worth noting here is that the controller class needs to be defined as abstract
 * and extend the \Magento\Framework\App\Action\Action class.
 * Controller actions live outside of the controller itself and can be found under the subdirectory
 * on the same level and named as controller. Since our controller is called Test,
 * we place our Crud action under the app/code/Bitkorn/Office/Controller/Test/Crud.php file
 *
 * @author allapow
 */
abstract class Test extends \Magento\Framework\App\Action\Action {
    
}
