
This Model is from eBook
Magento-2-Developers-Guide_Packt-2016-en
Kapitel 4 - Models and Collections

####enable the Module:

    php bin/magento module:enable --clear-static-content Bitkorn_Office
This will output:

    The following modules have been enabled:
    - Bitkorn_Bitkbackend
    
    To make sure that the enabled modules are properly registered, run 'setup:upgrade'.
    Cache cleared successfully.
    Generated classes cleared successfully. Please run the 'setup:di:compile' command to generate classes.
    Generated static view files cleared successfully.

also:

    php bin/magento setup:upgrade

und:

    php bin/magento setup:di:compile

Für den letzten Befehl muß das Verzeichnis /var/di gelöscht werden.

[Magento Doc: Enable or disable your component](http://devdocs.magento.com/guides/v2.0/extension-dev-guide/build/enable-module.html)