<?php

namespace Bitkorn\Office\Block;

/**
 * extends an important block type (Magento\Framework\View\Element\Template)
 * ...dieser implementiert _toHtml() und verwendet ein Template
 * ...darum funzt dieser Block nur mit einem Template (Bitkorn/Office/view/frontend/templates/office/hello.phtml)
 * ...das im Layout Bitkorn/Office/view/frontend/layout/bitkorn_office_test_crud.xml mit dem Block hier verbunden ist.
 * Das Layout folgt der Namenskonvention und wird dadurch bei der Route /bitkorn_office/Test/crud verwendet.
 */
class Hello extends \Magento\Framework\View\Element\Template
{
    public function helloPublic()
    {
        return 'Hello #1 ...from public';
    }

    /**
     * NICHT im Template aufzurufen!
     * @return string
     */
    protected function helloProtected()
    {
        return 'Hello #2 ...from protected';
    }

    /**
     * NICHT im Template aufzurufen!
     * @return string
     */
    private function helloPrivate()
    {
        return 'Hello #3 ...from private';
    }
}