<?php

/*
 * no license
 */

namespace Bitkorn\Office\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Description of InstallData
 *
 * @author allapow
 */
class InstallData implements InstallDataInterface {

    private $employeeSetupFactory;

    /**
     * \Bitkorn\Office\Setup\EmployeeSetupFactory wird per Dependency Injection mit "/bin magento setup:di:compile" erstellt
     * @param \Bitkorn\Office\Setup\EmployeeSetupFactory $employeeSetupFactory
     */
    public function __construct(
    \Bitkorn\Office\Setup\EmployeeSetupFactory $employeeSetupFactory
    ) {
        $this->employeeSetupFactory = $employeeSetupFactory;
    }

    /**
     * 
     * /vendor/magento/module-eav/Model/Entity/Setup/PropertyMapper.php
     * The key strings match the column names in the eav_attribute table,
     * while the value strings match the keys of our array passed to the
     * addAttribute method within InstallData.php.
     * 
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();
        $employeeEntity = \Bitkorn\Office\Model\Employee::ENTITY;

        $employeeSetup = $this->employeeSetupFactory->create(['setup' => $setup]);

        /*
         * create the row in db.eav_entity_type
         * with entity_table=bitkorn_office_employee_entity
         */
        $employeeSetup->installEntities();

        /*
         * create table rows in db.eav_attribute
         */
        $employeeSetup->addAttribute(
                $employeeEntity, 'service_years', ['type' => 'int']
        );

        $employeeSetup->addAttribute(
                $employeeEntity, 'dob', ['type' => 'datetime']
        );

        $employeeSetup->addAttribute(
                $employeeEntity, 'salary', ['type' => 'decimal']
        );

        $employeeSetup->addAttribute(
                $employeeEntity, 'vat_number', ['type' => 'varchar']
        );

        $employeeSetup->addAttribute(
                $employeeEntity, 'note', ['type' => 'text']
        );
        $setup->endSetup();
    }

}
