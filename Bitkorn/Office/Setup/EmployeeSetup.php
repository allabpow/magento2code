<?php

/*
 * no license
 */

namespace Bitkorn\Office\Setup;

use Magento\Eav\Setup\EavSetup;

/**
 * Das Buch will hier: class EmployeeSetupFactory
 * ...aber EmployeeSetupFactory wird per: "/bin magento setup:di:compile" erstellt
 * ...weils von EavSetup erbt???
 *
 * @author allapow
 */
class EmployeeSetup extends EavSetup {

    public function getDefaultEntities() {
        /**
         * Mit
         * SELECT * FROM eav_entity_type WHERE entity_type_code = "bitkorn_office_employee";
         * sollte es dann Ergebnisse geben.
         * @var string becomes an entry in the eav_entity_type table
         */
        $employeeEntity = \Bitkorn\Office\Model\Employee::ENTITY;
        $entities = [
            $employeeEntity => [
                'entity_model' => 'Bitkorn\Office\Model\ResourceModel\Employee',
                'table' => $employeeEntity . '_entity',
                'attributes' => [
                    'department_id' => [
                        'type' => 'static',
                    ],
                    'email' => [
                        'type' => 'static',
                    ],
                    'first_name' => [
                        'type' => 'static',
                    ],
                    'last_name' => [
                        'type' => 'static',
                    ],
                ],
            ],
        ];
        return $entities;
    }

}
