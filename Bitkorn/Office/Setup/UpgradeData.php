<?php

/*
 * no license
 */

namespace Bitkorn\Office\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Description of UpgradeData
 *
 * @author allapow
 */
class UpgradeData implements UpgradeDataInterface {

    protected $departmentFactory;
    protected $employeeFactory;

    /**
     * \Bitkorn\Office\Model\DepartmentFactory und \Bitkorn\Office\Model\EmployeeFactory werden per Dependency Injection mit "/bin magento setup:di:compile" erstellt
     * @param \Bitkorn\Office\Model\DepartmentFactory $departmentFactory
     * @param \Bitkorn\Office\Model\EmployeeFactory $employeeFactory
     */
    public function __construct(
    \Bitkorn\Office\Model\DepartmentFactory $departmentFactory, \Bitkorn\Office\Model\EmployeeFactory $employeeFactory
    ) {
        $this->departmentFactory = $departmentFactory;
        $this->employeeFactory = $employeeFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();
        $salesDepartment = $this->departmentFactory->create();
        $salesDepartment->setName('Sales');
        $salesDepartment->save();

        $employee = $this->employeeFactory->create();
        $employee->setDepartmentId($salesDepartment->getId());
        $employee->setEmail('john@sales.loc');
        $employee->setFirstName('John');
        $employee->setLastName('Doe');
        /*
         * folgende Daten haben keine Tabellenspalten-Entsprechungen
         * und verursachen Fehler bei: bin/magento setup:upgrade
         * Base table or view not found: 1146 Table 'magento200examples.bitkorn_office_employee_entity_int' doesn't exist, query was: DESCRIBE `bitkorn_office_employee_entity_int`
         * Loesung: in \Bitkorn\Office\Setup\InstallSchema die bitkorn_office_employee_entity_* Tables erzeugen
         */
        $employee->setServiceYears(3);
        $employee->setDob('1983-03-28');
        $employee->setSalary(3800.00);
        $employee->setVatNumber('GB123456789');
        $employee->setNote('Just some notes about John');
        $employee->save();
        $setup->endSetup();
    }

}
