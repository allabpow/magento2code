<?php

namespace Bitkorn\Bitkbackend\Model\ResourceModel\Merchant;

/**
 * Description of Collection
 *
 * @author allapow
 */
class Collection extends \Magento\Eav\Model\Entity\Collection\AbstractCollection {

    protected function _construct() {
        // erster Parameter ist das einfache Model
        // zweiter Parameter ist das ResourceModel
        $this->_init('Bitkorn\Bitkbackend\Model\Merchant', 'Bitkorn\Bitkbackend\Model\ResourceModel\Merchant');
    }

}
