<?php

/*
 * no license
 */
namespace Bitkorn\Bitkbackend\Model\ResourceModel;

/**
 * Description of Merchant
 *
 * @author allapow
 */
class Merchant extends \Magento\Eav\Model\Entity\AbstractEntity {

    protected function _construct() {
        // set the $this->_read, $this->_write class properties
        // These are freely assigned to whatever value we want, preferably following the naming pattern of our module.
        $this->_read  = 'bitkorn_bitkbackend_merchant_read';
        $this->_write = 'bitkorn_bitkbackend_merchant_write';
    }

    public function getEntityType() {
        if (empty($this->_type)) {
            $this->setType(\Bitkorn\Bitkbackend\Model\Merchant::ENTITY);
        }
        return parent::getEntityType();
    }
}

//class Merchant extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
//
//    protected function _construct() {
//        $this->_init('bitkorn_bitkbackend_merchant_entity', 'entity_id');
//    }
//}
