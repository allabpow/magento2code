<?php

/*
 * no license
 */
namespace Bitkorn\Bitkbackend\Model;

/**
 * Description of Merchant
 *
 * @author allapow
 */
class Merchant extends \Magento\Framework\Model\AbstractModel {

    /**
     * @var string This value is what's stored in the entity_type_code column within the eav_entity_type table
     */
    const ENTITY = 'bitkorn_bitkbackend_merchant';

    public function _construct() {
        // auch das EAV Model initialisiert sich mit seinem ResourceModel
        $this->_init('Bitkorn\Bitkbackend\Model\ResourceModel\Merchant');
    }
}
