<?php

/*
 * no license
 */

namespace Bitkorn\Bitkbackend\Setup;

use Magento\Eav\Setup\EavSetup;

/**
 * Das Buch will hier: class EmployeeSetupFactory
 * ...aber EmployeeSetupFactory wird per: "/bin magento setup:di:compile" erstellt
 * ...weils von EavSetup erbt???
 *
 * @author allapow
 */
class MerchantSetup extends EavSetup {

    public function getDefaultEntities() {
        /**
         * Mit
         * SELECT * FROM eav_entity_type WHERE entity_type_code = "bitkorn_bitkbackend_merchant";
         * sollte es dann Ergebnisse geben ...genau 1
         * @var string becomes an entry in the eav_entity_type table
         */
        $entityPrefix = \Bitkorn\Bitkbackend\Model\Merchant::ENTITY;
        $entities = [
            $entityPrefix => [
                'entity_model' => 'Bitkorn\Bitkbackend\Model\ResourceModel\Merchant',
                'table' => $entityPrefix . '_entity',
                'attributes' => [
                    'merchant_id' => [
                        'type' => 'static',
                    ],
                    'email' => [
                        'type' => 'static',
                    ],
                    'first_name' => [
                        'type' => 'static',
                    ],
                    'last_name' => [
                        'type' => 'static',
                    ],
                    'shop_name' => [
                        'type' => 'static',
                    ],
                ],
            ],
        ];
        return $entities;
    }

}
