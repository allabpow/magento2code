<?php

namespace Bitkorn\Bitkbackend\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Description of InstallSchema
 *
 * @author allapow
 */
class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();        
        
        $entityPrefix = \Bitkorn\Bitkbackend\Model\Merchant::ENTITY;
        $table01 = $setup->getConnection()
                ->newTable($setup->getTable($entityPrefix . '_entity'))
                ->addColumn(
                        // identity macht auto increment
                        'entity_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Entity ID'
                )
                ->addColumn(
                        'merchant_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Merchant ID'
                )
                ->addColumn(
                        'shop_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Shop Name'
                )
                ->addColumn(
                        'email', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 64, [], 'Email'
                )
                ->addColumn(
                        'first_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 64, [], 'First Name'
                )
                ->addColumn(
                        'last_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 64, [], 'Last Name'
                )
                ->setComment('Bitkorn Bitkbackend Merchant Table');
        $setup->getConnection()->createTable($table01);
        
        /*
         * weil /app/code/Bitkorn/Office/Setup/InstallData.php noch Attributes hinzu fuegt:
         * Depending on the EAV attribute data type, we need to create the following tables:
         * bitkorn_office_employee_entity_datetime
         * bitkorn_office_employee_entity_decimal
         * bitkorn_office_employee_entity_int
         * bitkorn_office_employee_entity_text
         * bitkorn_office_employee_entity_varchar
         * The names of these attribute value tables come from a simple formula, which says {name of the entity table}+{_}+{eav_attribute.backend_type value}.
         */
//        $table03 = $setup->getConnection()
//                ->newTable($setup->getTable($entityPrefix . '_entity_decimal'))
//                ->addColumn(
//                        'value_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'Value ID'
//                )
//                ->addColumn(
//                        'attribute_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Attribute ID'
//                )
//                ->addColumn(
//                        'store_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Store ID'
//                )
//                ->addColumn(
//                        'entity_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Entity ID'
//                )
//                ->addColumn(
//                        'value', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '12,4', [], 'Value'
//                )
//                ->addIndex(
//                        $setup->getIdxName(
//                                $entityPrefix . '_entity_decimal', ['entity_id', 'attribute_id', 'store_id'], \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
//                        ), ['entity_id', 'attribute_id', 'store_id'], ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
//                )
//                ->addIndex(
//                        $setup->getIdxName($entityPrefix . '_entity_decimal', ['store_id']), ['store_id']
//                )
//                ->addIndex(
//                        $setup->getIdxName($entityPrefix . '_entity_decimal', ['attribute_id']), ['attribute_id']
//                )
//                ->addForeignKey(
//                        $setup->getFkName(
//                                $entityPrefix . '_entity_decimal', 'attribute_id', 'eav_attribute', 'attribute_id'
//                        ), 'attribute_id', $setup->getTable('eav_attribute'), 'attribute_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->addForeignKey(
//                        $setup->getFkName(
//                                $entityPrefix . '_entity_decimal', 'entity_id', $entityPrefix . '_entity', 'entity_id'
//                        ), 'entity_id', $setup->getTable($entityPrefix . '_entity'), 'entity_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->addForeignKey(
//                        $setup->getFkName($entityPrefix . '_entity_decimal', 'store_id', 'store', 'store_id'), 'store_id', $setup->getTable('store'), 'store_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->setComment('Employee Decimal Attribute Backend Table');
//        $setup->getConnection()->createTable($table03);
//
//        $table04 = $setup->getConnection()
//                ->newTable($setup->getTable($entityPrefix . '_entity_datetime'))
//                ->addColumn(
//                        'value_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'Value ID'
//                )
//                ->addColumn(
//                        'attribute_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Attribute ID'
//                )
//                ->addColumn(
//                        'store_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Store ID'
//                )
//                ->addColumn(
//                        'entity_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Entity ID'
//                )
//                ->addColumn(
//                        'value', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, [], 'Value'
//                )
//                ->addIndex(
//                        $setup->getIdxName(
//                                $entityPrefix . '_entity_datetime', ['entity_id', 'attribute_id', 'store_id'], \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
//                        ), ['entity_id', 'attribute_id', 'store_id'], ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
//                )
//                ->addIndex(
//                        $setup->getIdxName($entityPrefix . '_entity_datetime', ['store_id']), ['store_id']
//                )
//                ->addIndex(
//                        $setup->getIdxName($entityPrefix . '_entity_datetime', ['attribute_id']), ['attribute_id']
//                )
//                ->addForeignKey(
//                        $setup->getFkName(
//                                $entityPrefix . '_entity_datetime', 'attribute_id', 'eav_attribute', 'attribute_id'
//                        ), 'attribute_id', $setup->getTable('eav_attribute'), 'attribute_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->addForeignKey(
//                        $setup->getFkName(
//                                $entityPrefix . '_entity_datetime', 'entity_id', $entityPrefix . '_entity', 'entity_id'
//                        ), 'entity_id', $setup->getTable($entityPrefix . '_entity'), 'entity_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->addForeignKey(
//                        $setup->getFkName($entityPrefix . '_entity_datetime', 'store_id', 'store', 'store_id'
//                        ), 'store_id', $setup->getTable('store'), 'store_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->setComment('Employee Datetime Attribute Backend Table');
//        $setup->getConnection()->createTable($table04);
//
//        $table05 = $setup->getConnection()
//                ->newTable($setup->getTable($entityPrefix . '_entity_int'))
//                ->addColumn(
//                        'value_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'Value ID'
//                )
//                ->addColumn(
//                        'attribute_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Attribute ID'
//                )
//                ->addColumn(
//                        'store_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Store ID'
//                )
//                ->addColumn(
//                        'entity_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Entity ID'
//                )
//                ->addColumn(
//                        'value', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [], 'Value'
//                )
//                ->addIndex(
//                        $setup->getIdxName(
//                                $entityPrefix . '_entity_int', ['entity_id', 'attribute_id', 'store_id'], \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
//                        ), ['entity_id', 'attribute_id', 'store_id'], ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
//                )
//                ->addIndex(
//                        $setup->getIdxName($entityPrefix . '_entity_int', ['store_id']), ['store_id']
//                )
//                ->addIndex(
//                        $setup->getIdxName($entityPrefix . '_entity_int', ['attribute_id']), ['attribute_id']
//                )
//                ->addForeignKey(
//                        $setup->getFkName(
//                                $entityPrefix . '_entity_int', 'attribute_id', 'eav_attribute', 'attribute_id'
//                        ), 'attribute_id', $setup->getTable('eav_attribute'), 'attribute_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->addForeignKey(
//                        $setup->getFkName(
//                                $entityPrefix . '_entity_int', 'entity_id', $entityPrefix . '_entity', 'entity_id'
//                        ), 'entity_id', $setup->getTable($entityPrefix . '_entity'), 'entity_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->addForeignKey(
//                        $setup->getFkName($entityPrefix . '_entity_int', 'store_id', 'store', 'store_id'
//                        ), 'store_id', $setup->getTable('store'), 'store_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->setComment('Employee Integer Attribute Backend Table');
//        $setup->getConnection()->createTable($table05);
//
//        $table06 = $setup->getConnection()
//                ->newTable($setup->getTable($entityPrefix . '_entity_text'))
//                ->addColumn(
//                        'value_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'Value ID'
//                )
//                ->addColumn(
//                        'attribute_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Attribute ID'
//                )
//                ->addColumn(
//                        'store_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Store ID'
//                )
//                ->addColumn(
//                        'entity_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Entity ID'
//                )
//                ->addColumn(
//                        'value', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, [], 'Value'
//                )
//                ->addIndex(
//                        $setup->getIdxName(
//                                $entityPrefix . '_entity_text', ['entity_id', 'attribute_id', 'store_id'], \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
//                        ), ['entity_id', 'attribute_id', 'store_id'], ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
//                )
//                ->addIndex(
//                        $setup->getIdxName($entityPrefix . '_entity_text', ['store_id']), ['store_id']
//                )
//                ->addIndex(
//                        $setup->getIdxName($entityPrefix . '_entity_text', ['attribute_id']), ['attribute_id']
//                )
//                ->addForeignKey(
//                        $setup->getFkName(
//                                $entityPrefix . '_entity_text', 'attribute_id', 'eav_attribute', 'attribute_id'
//                        ), 'attribute_id', $setup->getTable('eav_attribute'), 'attribute_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->addForeignKey(
//                        $setup->getFkName(
//                                $entityPrefix . '_entity_text', 'entity_id', $entityPrefix . '_entity', 'entity_id'
//                        ), 'entity_id', $setup->getTable($entityPrefix . '_entity'), 'entity_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->addForeignKey(
//                        $setup->getFkName($entityPrefix . '_entity_text', 'store_id', 'store', 'store_id'
//                        ), 'store_id', $setup->getTable('store'), 'store_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->setComment('Employee text Attribute Backend Table');
//        $setup->getConnection()->createTable($table06);
//
//        $table07 = $setup->getConnection()
//                ->newTable($setup->getTable($entityPrefix . '_entity_varchar'))
//                ->addColumn(
//                        'value_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'Value ID'
//                )
//                ->addColumn(
//                        'attribute_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Attribute ID'
//                )
//                ->addColumn(
//                        'store_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Store ID'
//                )
//                ->addColumn(
//                        'entity_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Entity ID'
//                )
//                ->addColumn(
//                        'value', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'Value'
//                )
//                ->addIndex(
//                        $setup->getIdxName(
//                                $entityPrefix . '_entity_varchar', ['entity_id', 'attribute_id', 'store_id'], \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
//                        ), ['entity_id', 'attribute_id', 'store_id'], ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
//                )
//                ->addIndex(
//                        $setup->getIdxName($entityPrefix . '_entity_varchar', ['store_id']), ['store_id']
//                )
//                ->addIndex(
//                        $setup->getIdxName($entityPrefix . '_entity_varchar', ['attribute_id']), ['attribute_id']
//                )
//                ->addForeignKey(
//                        $setup->getFkName(
//                                $entityPrefix . '_entity_varchar', 'attribute_id', 'eav_attribute', 'attribute_id'
//                        ), 'attribute_id', $setup->getTable('eav_attribute'), 'attribute_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->addForeignKey(
//                        $setup->getFkName(
//                                $entityPrefix . '_entity_varchar', 'entity_id', $entityPrefix . '_entity', 'entity_id'
//                        ), 'entity_id', $setup->getTable($entityPrefix . '_entity'), 'entity_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->addForeignKey(
//                        $setup->getFkName($entityPrefix . '_entity_varchar', 'store_id', 'store', 'store_id'
//                        ), 'store_id', $setup->getTable('store'), 'store_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//                )
//                ->setComment('Employee Varchar Attribute Backend Table');
//        $setup->getConnection()->createTable($table07);

        $setup->endSetup();
    }

}
