<?php

/*
 * no license
 */

namespace Bitkorn\Bitkbackend\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Description of UpgradeData
 *
 * @author allapow
 */
class UpgradeData implements UpgradeDataInterface {

    protected $merchantFactory;
    
    /**
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * \Bitkorn\Office\Model\DepartmentFactory und \Bitkorn\Office\Model\EmployeeFactory werden per Dependency Injection mit "/bin magento setup:di:compile" erstellt
     * @param \Bitkorn\Office\Model\DepartmentFactory $departmentFactory
     * @param \Bitkorn\Office\Model\EmployeeFactory $employeeFactory
     */
    public function __construct(
    \Bitkorn\Bitkbackend\Model\MerchantFactory $merchantFactory, \Psr\Log\LoggerInterface $logger
    ) {
        $this->merchantFactory = $merchantFactory;
        $this->_logger = $logger;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
//        $this->_logger->debug('Context get Version: ' . $context->getVersion()); // gibt keine Version, sondern: Context get Version:  {"is_exception":false} []
//        if (version_compare($context->getVersion(), '0.0.1') < 0) {
//        // Seit Magento 2.1 wirft das hier n Fehler
//            $setup->startSetup();
//            $defaultMerchant = $this->merchantFactory->create();
//            $defaultMerchant->setEmail('Default');
//            $defaultMerchant->setFirstName('Torsten');
//            $defaultMerchant->setLastName('Brieskorn');
//            $defaultMerchant->setShopName('Amandla Shop');
//            $defaultMerchant->save();
//
//            $setup->endSetup();
//        }
    }

}
