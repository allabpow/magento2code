<?php

/*
 * no license
 */

namespace Bitkorn\Bitkbackend\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Description of InstallData
 *
 * @author allapow
 */
class InstallData implements InstallDataInterface {

    private $merchantSetupFactory;
    
    /**
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * \Bitkorn\Bitkbackend\Setup\MerchantSetupFactory wird per Dependency Injection mit "/bin magento setup:di:compile" erstellt
     * @param \Bitkorn\Bitkbackend\Setup\MerchantSetupFactory $merchantSetupFactory
     */
    public function __construct(
    \Bitkorn\Bitkbackend\Setup\MerchantSetupFactory $merchantSetupFactory, \Psr\Log\LoggerInterface $logger
    ) {
        $this->merchantSetupFactory = $merchantSetupFactory;
        $this->_logger = $logger;
    }

    /**
     * 
     * /vendor/magento/module-eav/Model/Entity/Setup/PropertyMapper.php
     * The key strings match the column names in the eav_attribute table,
     * while the value strings match the keys of our array passed to the
     * addAttribute method within InstallData.php.
     * 
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();
//        $entityPrefix = \Bitkorn\Bitkbackend\Model\Merchant::ENTITY;

        $merchantSetup = $this->merchantSetupFactory->create(['setup' => $setup]);

        /*
         * create the row in db.eav_entity_type
         * with entity_table=bitkorn_bitkbackend_merchant_entity
         */
        $merchantSetup->installEntities();

        /*
         * create table rows in db.eav_attribute
         */
//        $merchantSetup->addAttribute(
//                $entityPrefix, 'service_years', ['type' => 'int']
//        );
//
//        $merchantSetup->addAttribute(
//                $entityPrefix, 'dob', ['type' => 'datetime']
//        );
//
//        $merchantSetup->addAttribute(
//                $entityPrefix, 'salary', ['type' => 'decimal']
//        );
//
//        $merchantSetup->addAttribute(
//                $entityPrefix, 'vat_number', ['type' => 'varchar']
//        );
//
//        $merchantSetup->addAttribute(
//                $entityPrefix, 'note', ['type' => 'text']
//        );
        $setup->endSetup();
    }

}
