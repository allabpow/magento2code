<?php

/*
 * no license
 */
namespace Bitkorn\Bitkbackend\Controller\Adminhtml\Merchants;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Index Controller class
 *
 * @author allapow
 */
class Index extends \Magento\Backend\App\Action {

    protected $resultPageFactory;

    public function __construct(
    Context $context, PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    
    public function execute() {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Bitkorn_Bitkbackend::merchants');
        $resultPage->addBreadcrumb(__('Bitkbackend'), __('Bitkbackend'));
        $resultPage->addBreadcrumb(__('List Merchants'), __('List Merchants'));
        $resultPage->getConfig()->getTitle()->prepend(__('Merchants'));
//        \Zend_Debug::dump($resultPage);
        return $resultPage;
    }
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Bitkorn_Bitkbackend::merchants');
    }
}
