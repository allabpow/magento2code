<?php

/*
 * no license
 */

namespace Bitkorn\Bitkbackend\Controller\Adminhtml\Index;

/**
 * Index Action class
 *
 * @author allapow
 */
class Index extends \Bitkorn\Bitkbackend\Controller\Adminhtml\Index {

    protected $resultPageFactory;

    /**
     * 
     * @param \Magento\Backend\App\Action\Context $context siehe \Magento\Backend\App\AbstractAction. In den Buechern von Packt steht \Magento\Framework\App\Action\Context.
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute() {
        $resultPage = $this->resultPageFactory->create();

        return $resultPage;
    }
    
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Bitkorn_Bitkbackend::index');
    }
}
