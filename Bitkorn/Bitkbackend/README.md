
Teilweise aus dem Buch:
Magento-2-Development-Cookbook_Packt-2016-en

####enable the Module:

    php bin/magento module:enable --clear-static-content Bitkorn_Bitkbackend
This will output:

    The following modules have been enabled:
    - Bitkorn_Bitkbackend
    
    To make sure that the enabled modules are properly registered, run 'setup:upgrade'.
    Cache cleared successfully.
    Generated classes cleared successfully. Please run the 'setup:di:compile' command to generate classes.
    Generated static view files cleared successfully.

also:

    php bin/magento setup:upgrade

und:

    php bin/magento setup:di:compile

Für den letzten Befehl muß das Verzeichnis /var/di gelöscht werden.
