<?php

/*
 * no license
 */

namespace Bitkorn\Bitkbackend\Block\Adminhtml;

/**
 * Description of Merchants
 *
 * @author allapow
 */
class Merchants extends \Magento\Backend\Block\Widget\Grid\Container {

    protected function _construct() {
        $this->_blockGroup = 'Bitkorn_Bitkbackend';
        $this->_controller = 'adminhtml_merchants';
        
        parent::_construct();
    }

}
