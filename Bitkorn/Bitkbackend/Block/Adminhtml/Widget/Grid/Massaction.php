<?php

namespace Bitkorn\Bitkbackend\Block\Adminhtml\Widget\Grid;

use Magento\Backend\Block\Widget\Grid\Massaction\AbstractMassaction;

/**
 * Description of Massaction
 *
 * @author allapow
 */
class Massaction extends AbstractMassaction
{
    protected $_template = 'Bitkorn_Bitkbackend::widget\grid\massaction.phtml';
    

}
