<?php

/*
 * no license
 */

namespace Bitkorn\Bitkbackend\Block\Adminhtml\Merchants;

use Magento\Backend\Block\Widget\Grid as WidgetGrid;

/**
 * Description of Grid
 *
 * extends \Magento\Backend\Block\Widget\Grid\Extended
 * 
 * @author allapow
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{

    /**
     * @var \Packt\HelloWorld\Model\Resource\Subscription\Collection
     */
    protected $_merchantCollection;
    
    /**
     *
     * @var string Path to my overridden template
     */
    protected $_template = 'Bitkorn_Bitkbackend::widget/grid/extended.phtml';

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Bitkorn\Bitkbackend\Model\ResourceModel\Merchant\Collection $merchantCollection
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Backend\Helper\Data $backendHelper,
            \Bitkorn\Bitkbackend\Model\ResourceModel\Merchant\Collection $merchantCollection, array $data = []
    )
    {
        $this->_merchantCollection = $merchantCollection;
        parent::__construct($context, $backendHelper, $data);
        $this->setEmptyText(__('No Merchant Found'));
    }

    /**
     * Initialize the merchant collection
     *
     * @return WidgetGrid
     */
    protected function _prepareCollection()
    {
        $this->setCollection($this->_merchantCollection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare grid columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->setMassactionIdField('entity_id');
        $this->setMassactionBlockName('Bitkorn\Bitkbackend\Block\Adminhtml\Widget\Grid\Massaction'); // Bitkorn\Bitkbackend\Block\Adminhtml\Widget\Grid\Massaction works

        $this->addColumn('entity_id',
                [
            'header' => __('ID'),
            'index' => 'entity_id',
            'width' => '60px'
                ]
        );
        $this->addColumn(
                'first_name', [
            'header' => __('Firstname'),
            'index' => 'first_name',
                ]
        );

        $this->addColumn(
                'last_name', [
            'header' => __('Lastname'),
            'index' => 'last_name',
                ]
        );

        $this->addColumn(
                'email', [
            'header' => __('Email address'),
            'index' => 'email',
                ]
        );

        $this->addColumn(
                'shop_name', [
            'header' => __('Status'),
            'index' => 'shop_name',
                ]
        );

        return $this;
    }

}
