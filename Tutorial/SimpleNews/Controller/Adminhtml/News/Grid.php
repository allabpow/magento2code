<?php

/*
 * This is the grid action which is used for loading grid by ajax
 */

namespace Tutorial\SimpleNews\Controller\Adminhtml\News;

class Grid
{

    /**
     * @return void
     */
    public function execute()
    {
        return $this->_resultPageFactory->create();
    }

}
