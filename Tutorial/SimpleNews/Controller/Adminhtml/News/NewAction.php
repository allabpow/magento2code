<?php

/*
 * This is the new action.
 * NewAction weil New ein Schluesselwort ist.
 */

namespace Tutorial\SimpleNews\Controller\Adminhtml\News;

use Tutorial\SimpleNews\Controller\Adminhtml\News;

class NewAction extends News
{

    /**
     * Create new news action
     *
     * @return void
     */
    public function execute()
    {
        /*
         * forward to
         * simplenews/news/edit
         * relativ zum aktuellen Modul und Namespace
         */
        $this->_forward('edit');
    }

}
