<?php

/*
 * This is the block file of grid container
 */

namespace Tutorial\SimpleNews\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class News extends Container
{

    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_news';
        $this->_blockGroup = 'Tutorial_SimpleNews';
        $this->_headerText = __('Manage News');
        /*
         * der add-Button (NICHT add einen Button mit Label)
         */
        $this->_addButtonLabel = __('Add News');
        parent::_construct();
    }

}
