<?php
/*
 * Fuer das hier wird eine Factory von Magento automatisch erstellt:
 * /var/generation/Tutorial/SimpleNews/Model/NewsFactory.php
 */
namespace Tutorial\SimpleNews\Model;

use Magento\Framework\Model\AbstractModel;

class News extends AbstractModel
{

    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Tutorial\SimpleNews\Model\Resource\News');
    }

}
